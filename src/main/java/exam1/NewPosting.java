package exam1;

import org.metaworks.annotation.AutowiredFromClient;
import org.metaworks.annotation.ServiceMethod;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;

// Posting을 상속 받는다.
public class NewPosting extends Posting {

    // 메세지를 포스팅하는 기능
    @ServiceMethod(callByContent = true, keyBinding = "enter")
    public Facebook post(@AutowiredFromClient Facebook facebook) {
        Posting posting = new Posting();

//        현재 클래스의 상태를 posting 객체에 복사
        try {
            BeanUtils.copyProperties(this, posting);
        } catch (BeansException e) {
            e.printStackTrace();
        }
//        posting.setMessage(this.getMessage());
//        사용자가 입력한 message를 feed에 추가
        facebook.getFeed().add(posting);

        facebook.setNewPosting(new NewPosting());
        return facebook;
    }

}
