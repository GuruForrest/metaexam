package exam1;

// 메세지를 담고 있는 기능은 동일하기 때문에
// 메세지를 저장하고 출력하는 기능을하는 객체는 Posting으로 정의 해준다
public class Posting {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
