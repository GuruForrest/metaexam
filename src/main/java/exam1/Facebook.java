package exam1;

import org.metaworks.annotation.Order;

import java.util.ArrayList;
import java.util.List;

/*
* 화면에 출력할 값들을 정의
* */
public class Facebook {
    //    데이터를 입력할 객체
    NewPosting newPosting = new NewPosting();

    //    데이터를 저장할 feed 객체
    List<Posting> feed = new ArrayList<Posting>();

    // Order : 데이터의 출력 순서를 지정한다
    @Order(1)
    public NewPosting getNewPosting() {
        return newPosting;
    }

    public void setNewPosting(NewPosting newPosting) {
        this.newPosting = newPosting;
    }

    @Order(2)
    public List<Posting> getFeed() {
        return feed;
    }

    public void setFeed(List<Posting> feed) {
        this.feed = feed;
    }
}
